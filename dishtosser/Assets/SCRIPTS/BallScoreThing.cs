using UnityEngine;
using System.Collections;

public class BallScoreThing : MonoBehaviour {
	
	Vector3 initial = Vector3.zero;
	// Use this for initialization
	void Start () {
		initial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
			if(transform.position.y < -10)
				restart();
	}
	
	public void restart()
	{
		
			transform.position = initial;
			rigidbody.velocity = Vector3.zero;
	}
	
	public void OnTriggerEnter(Collider col)
	{
		if(col.name.Contains("LEFT") || col.name.Contains( "RIGHT"))
		{
			
			restart ();
			UiScript.sInstance.addScore(!col.name.Contains("LEFT"));
		}
	}
}
