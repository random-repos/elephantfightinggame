using UnityEngine;
using System.Collections;

public class SoundReferences : MonoBehaviour {
	
	public static SoundReferences sInstance = null;
	public Texture2D logo;
	public AudioClip[] noises;
	void Start () {
		sInstance = this;
	}
	
}
