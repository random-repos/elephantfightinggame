using UnityEngine;
using System.Collections;

public class BadJoint : MonoBehaviour {
	
	public Rigidbody connectedBody;
	Quaternion tLocalRotation;
	Vector3	tLocalPosition;
	
	void Start () {
		Transform oldParent = transform.parent;
		if(connectedBody != null)
		{
			transform.parent = connectedBody.transform;
		} else transform.parent = null;
		tLocalPosition = transform.localPosition;
		tLocalRotation = transform.localRotation;
		transform.parent = oldParent;
	}
	
	// Update is called once per frame
	void Update () {
		Matrix4x4 m = Matrix4x4.identity;
		if(connectedBody != null)
			m = connectedBody.transform.localToWorldMatrix;
		Vector4 worldTarget4 = m*tLocalPosition;
		Vector3 worldTarget = new Vector3(worldTarget4.x,worldTarget4.y,worldTarget4.z);
		rigidbody.AddForce((worldTarget - transform.position)*1,ForceMode.Force);
	}
}
