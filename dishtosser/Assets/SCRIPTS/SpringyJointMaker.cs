using UnityEngine;
using System.Collections;

public class SpringyJointMaker : MonoBehaviour
{
	
	
	public int playerNumber;
	public Rigidbody bottom;
	GameObject[] stack;
	Vector3 restartPosiiton;
	Quaternion restartOrientation;
	int count = 8;
	Vector3 starting = new Vector3(0,1.3f,0);
	Vector3 increment = new Vector3(0,0.6f,0);
	float startingSpring = 15000000;
	float springGain = 1/4f;
	float startingMass = 5.0f;
	float massGain = 1.1f;
	
	float baseMase = 100;
	
	Color32 color;
	
	
	void Start()
	{
		bottom.rigidbody.mass = baseMase;
		
		if(playerNumber == 0)
			color = new Color32(112,0,204,200);
		else
			color = new Color32(222,111,161,200);
		
		foreach(Renderer e in bottom.GetComponentsInChildren<Renderer>())
			e.material.color = color;
		stack = new GameObject[10];
		for(int i = 0; i < count; i++)
		{
			stack[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			stack[i].name = "trunk";
			stack[i].AddComponent<ElephantSoundMaker>();
			stack[i].transform.position = bottom.transform.position + starting + increment*i;
			stack[i].AddComponent<Rigidbody>().mass = startingMass * Mathf.Pow(massGain,i);
			stack[i].renderer.material.color = color;
			stack[i].transform.parent = bottom.transform;
			restartPosiiton = bottom.transform.position;
			restartOrientation = bottom.transform.rotation;
			
			var joint = stack[i].AddComponent<ConfigurableJoint>();
			if(i > 0) joint.connectedBody = stack[i-1].rigidbody;
			else joint.connectedBody = bottom;
			
			joint.xMotion = ConfigurableJointMotion.Locked;
			joint.yMotion = ConfigurableJointMotion.Locked;
			joint.zMotion = ConfigurableJointMotion.Locked;
			joint.angularXMotion = ConfigurableJointMotion.Limited;
			joint.angularYMotion = ConfigurableJointMotion.Locked;
			joint.angularZMotion = ConfigurableJointMotion.Limited;
			SoftJointLimit[] limits = new SoftJointLimit[]{joint.lowAngularXLimit,joint.highAngularXLimit,joint.angularYLimit,joint.angularZLimit};
			for(int j = 0; j < limits.Length; j++)
			{
				limits[j].spring = startingSpring * Mathf.Pow(springGain,i);
				limits[j].bounciness = 0;
			}
			
		}
	}
	
	void Update()
	{
		
		KeyCode[] keyMappings = null;
		if(playerNumber == 0)
			keyMappings = new KeyCode[4]{KeyCode.LeftArrow,KeyCode.RightArrow,KeyCode.UpArrow,KeyCode.DownArrow};
		if(playerNumber == 1)
			keyMappings = new KeyCode[4]{KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.S};
		
		float speed = 2000;
		float rotSpeed =300;
		
		//TODO don't allow turning if already turning too fast..
		if(Input.GetKey(keyMappings[0]))
		{
			//bottom.AddForce(Vector3.left*speed);
			bottom.AddTorque(-Vector3.up*rotSpeed);
		}
		if(Input.GetKey(keyMappings[1]))
		{
			//bottom.AddForce(Vector3.right*speed);
			bottom.AddTorque(Vector3.up*rotSpeed);
		}
		
		Vector3 fwd = Vector3.Exclude(Vector3.up,bottom.transform.forward);
		
		if(fwd.sqrMagnitude < 0.1f)
			fwd = Vector3.Exclude(Vector3.up,bottom.transform.up).normalized;
		else fwd = fwd.normalized;
		
		
		
		
		
		//stay up force
		float stayupForce = 5000;
		float angle = Mathf.Acos(Vector3.Dot(bottom.transform.up,Vector3.up));
		Vector3 upward = Vector3.Cross(bottom.transform.up, Vector3.up).normalized;
		if( angle > Mathf.PI)
			angle = Mathf.PI;
		float pForce = (1-(Mathf.Abs(angle/Mathf.PI - .5f))*2);
		//float pForce = Mathf.Clamp(angle/Mathf.PI,0,0.7f);
		
		bottom.AddTorque(stayupForce * pForce * upward);
		
		
		//Debug.DrawLine(bottom.transform.position,bottom.transform.position + fwd * 5);
		
		if(Input.GetKey(keyMappings[2]))
		{
			bottom.AddForce(fwd*speed);
			bottom.AddTorque(5000 * pForce * upward);
		}
		if(Input.GetKey(keyMappings[3]))
		{
			bottom.AddForce(-fwd*speed);
		}
		
		
		if(bottom.transform.position.y < -10)
		{
			bottom.transform.position = restartPosiiton;
			bottom.transform.rotation = restartOrientation;
		}
	}
	
}
