using UnityEngine;
using System.Collections;

public class UiScript : MonoBehaviour {
	
	public static UiScript sInstance = null;
	
	bool showLogo = true;
	
	int leftScore = 0;
	int rightScore = 0;
	
	public void Start()
	{
		sInstance = this;
	}
	
	void OnGUI()
	{
		float textWidth = 200;
		
		GUIStyle style = new GUIStyle();
		style.fontSize = 30;
		style.normal.textColor = new Color(1,1,0,1);
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Box(new Rect(0,0,textWidth,100),"" + leftScore,style);	
		GUI.Box(new Rect(Screen.width - textWidth,0,textWidth,100),"" + rightScore,style);
		
		if (Event.current.type == EventType.KeyDown) 
        	showLogo = false;
			
		if(showLogo)
		{
			
			float padding = 200;
			GUI.Box(new Rect(padding,padding,Screen.width-padding*2,Screen.height-padding*2),SoundReferences.sInstance.logo,style);
		}
	}
	
	
	public void addScore(bool left)
	{
		if(left)
			leftScore++;
		else rightScore++;
		
		if(leftScore > 2 || rightScore > 2)
		{
			//TODO show winner..
			
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
