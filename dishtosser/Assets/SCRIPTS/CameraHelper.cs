using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraHelper : MonoBehaviour {
    public GameObject lookAt;
	
	
	
	
    void LateUpdate() {
		
		
		Camera cam = camera;
		
		float h = lookAt.transform.localScale.z*10f;
		float w = lookAt.transform.localScale.x*10f;
		float scaleRatio = 1.5f;
		
		float dToPlane = Mathf.Abs((lookAt.transform.position - cam.transform.position).y);
		float nearHeight = h*scaleRatio;
		//float focus = nearHeight*dToPlane/(nearHeight-h);
		float focus = -dToPlane/(h/nearHeight -1);
		
	
		
		//cam.aspect = w/h;
		//cam.fieldOfView = -Mathf.Atan2(w,focus);
		
        //Debug.Log(focus);
        Matrix4x4 m = funky( 0,dToPlane+10, focus, w*scaleRatio/2, h*scaleRatio/2);
		//Matrix4x4 m = PerspectiveOffCenter(left, right, bottom, top, cam.nearClipPlane, cam.farClipPlane);
		//Matrix4x4 m = cam.projectionMatrix;
		
		Matrix4x4 im = cam.transform.localToWorldMatrix*m.inverse;
		
		Debug.DrawLine(im*new Vector4(-1,-1,0,1),im*new Vector4(1,-1,0,1));
		Debug.DrawLine(im*new Vector4(-1,-1,0,1),im*new Vector4(-1,1,0,1));
		Debug.DrawLine(im*new Vector4(1,1,0,1),im*new Vector4(1,-1,0,1));
		Debug.DrawLine(im*new Vector4(1,1,0,1),im*new Vector4(-1,1,0,1));
			
		Debug.DrawLine(im*new Vector4(-1,-1,-1,1),im*new Vector4(1,-1,-1,1));
		Debug.DrawLine(im*new Vector4(-1,-1,-1,1),im*new Vector4(-1,1,-1,1));
		Debug.DrawLine(im*new Vector4(1,1,-1,1),im*new Vector4(1,-1,-1,1));
		Debug.DrawLine(im*new Vector4(1,1,-1,1),im*new Vector4(-1,1,-1,1));
		
        cam.projectionMatrix = m;
    }
    static Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near, float far) {
        float x = 2.0F * near / (right - left);
        float y = 2.0F * near / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0F * far * near) / (far - near);
        float e = -1.0F;
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x;
        m[0, 1] = 0;
        m[0, 2] = a;
        m[0, 3] = 0;
        m[1, 0] = 0;
        m[1, 1] = y;
        m[1, 2] = b;
        m[1, 3] = 0;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = c;
        m[2, 3] = d;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = e;
        m[3, 3] = 0;
        return m;
    }
	
	Matrix4x4 funky(float near, float far, float focus, float width, float height) 
	{
		
		float x = 1/width;
        float y = 1/height;
        float a = 0; //trans
        float b = 0; //trans
        //float c = -1/(far - near); //depth
        //float d = near / (far - near); //depth
		float c = ((near+far)/focus - 2)/(far-near);
        float d = (near*far*2/focus - far - near)/(far-near);
        float e = 1/(focus); //scale
		float f = 1;	//scale
		
		Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x; m[0, 1] = 0; m[0, 2] = a; m[0, 3] = 0;
        m[1, 0] = 0; m[1, 1] = y; m[1, 2] = b; m[1, 3] = 0;
		m[2, 0] = 0; m[2, 1] = 0; m[2, 2] = c; m[2, 3] = d;
        m[3, 0] = 0; m[3, 1] = 0; m[3, 2] = e; m[3, 3] = f;
        return m;
	}
	
	
}