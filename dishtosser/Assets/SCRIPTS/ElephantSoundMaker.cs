using UnityEngine;
using System.Collections;

public class ElephantSoundMaker : MonoBehaviour {
	
	
	public static AudioSource sSource = null;
	// Use this for initialization
	void Start () {
		if(sSource == null)
		{
			sSource = (new GameObject("source")).AddComponent<AudioSource>();
			sSource.loop = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void OnCollisionEnter(Collision col)
	{
		if(
			col.collider.attachedRigidbody != null &&
			(col.collider.attachedRigidbody.gameObject.name.Contains("trunk") ||
			col.collider.attachedRigidbody.gameObject.name.Contains("player"))
			)
		{
			if(collider.attachedRigidbody.velocity.sqrMagnitude < col.collider.attachedRigidbody.velocity.sqrMagnitude)
			{
				if(!sSource.isPlaying)
				{
					sSource.clip = SoundReferences.sInstance.noises[Random.Range(0,SoundReferences.sInstance.noises.Length)];
					sSource.Play();
				}
			}
		}
	}
}
